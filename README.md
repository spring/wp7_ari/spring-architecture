# SPRING Architecture

This repository contains high-level documentation of the H2020 SPRING
architecture, as well as mock ROS nodes for all the architecture modules.

![Screenshot of the architecture, displayed in Boxology](doc/architecture-boxology.jpg)

You can view a textual version of the architecture here:
[spring-architecture.md](spring-architecture.md).

The [architecture.json](architecture.json) file contains the details of the
nodes and interfaces in `json` format, and can be visualised with [the SPRING
branch of Boxology](https://github.com/severin-lemaignan/boxology/tree/spring).
You can [download a binary for Linux
here](https://github.com/severin-lemaignan/boxology/releases), or compile it
yourself. Note that Boxology tends to crash often! Ctrl+S is your friend!


## Running the mock nodes

### Using Docker

Launch the full SPRING architecture with:

```
docker run -it --network=host registry.gitlab.inria.fr/spring/dockers/spring-base roslaunch springarchitecture start_all.launch
```

This will start all the nodes visible in the architecture diagram above, either
the mock ones (if the node prefixed by `MOCK` in the diagram), or the actual
ones.

The ROS nodes are visible from the host system as well. For instnace, in a different terminal, try:

```
rostopic list
```

Replace `start_all.launch` by eg `start_all_INRIA.launch` to start all the
SPRING nodes *except* eg INRIA's ones (replace INRIA by your institution). This
makes it easy to test your own nodes while running the code of the partners 'as
it'.

### Compile manually

[spring-mock-ws](spring-mock-ws) is a regular `catkin` workspace that contains
all the mocked-up nodes, and a script to fetch the released ones.

To build it:

```
cd spring-mock-ws
sh ./fetch_released_nodes.sh # this requires SSH access to gitlab.inria.fr
catkin build
source devel/setup.bash
```

You should then have all the SPRING nodes ready to be used.
