/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "audio_common_msgs/AudioData.h"

class SpeakerIdentification {
   public:
    SpeakerIdentification(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        trackinginformation_sub_ = nh->subscribe("trackinginformation", 1, &SpeakerIdentification::trackinginformationCallback, this);
        postprocess_audio_streams_sub_ = nh->subscribe("postprocess_audio_streams", 1, &SpeakerIdentification::postprocess_audio_streamsCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        person_id_candidatehri_msgsidsmatch_pub_ = nh->advertise<std_msgs::Empty>("person_id_candidatehri_msgsidsmatch", 1);
        voice_audio_pub_ = nh->advertise<audio_common_msgs::AudioData>("voice_audio", 1);
    }

    ~SpeakerIdentification() {}

   private:
    void trackinginformationCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("speakeridentification: received message: " << msg);
    }
    void postprocess_audio_streamsCallback(const audio_common_msgs::AudioData::ConstPtr& msg)
    {
        ROS_INFO_STREAM("speakeridentification: received message: " << msg);
    }

    ros::Subscriber trackinginformation_sub_;
    ros::Subscriber postprocess_audio_streams_sub_;

    ros::Publisher person_id_candidatehri_msgsidsmatch_pub_;
    ros::Publisher voice_audio_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "speakeridentification");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto speakeridentification = SpeakerIdentification(&private_node_handle_);

    ROS_INFO("Node speakeridentification launched and ready.");
    ros::spin();
    return 0;
}
