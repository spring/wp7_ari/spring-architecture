/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"

class Interaction_manager {
   public:
    Interaction_manager(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        semanticscenedescription_sub_ = nh->subscribe("semanticscenedescription", 1, &Interaction_manager::semanticscenedescriptionCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        input_sub_ = nh->subscribe("input", 1, &Interaction_manager::inputCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        dialoguestate_sub_ = nh->subscribe("dialoguestate", 1, &Interaction_manager::dialoguestateCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        robotstate_sub_ = nh->subscribe("robotstate", 1, &Interaction_manager::robotstateCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        tf_sub_ = nh->subscribe("tf", 1, &Interaction_manager::tfCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        hppersonid_sub_ = nh->subscribe("hppersonid", 1, &Interaction_manager::hppersonidCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        whotolookat_pub_ = nh->advertise<std_msgs::Empty>("whotolookat", 1);
        // ATTENTION: this topic is not defined in the architecture design
        verbalcommand_pub_ = nh->advertise<std_msgs::Empty>("verbalcommand", 1);
        // ATTENTION: this topic is not defined in the architecture design
        navgoals_pub_ = nh->advertise<std_msgs::Empty>("navgoals", 1);
        // ATTENTION: this topic is not defined in the architecture design
        gestures_pub_ = nh->advertise<std_msgs::Empty>("gestures", 1);
        // ATTENTION: this topic is not defined in the architecture design
        activepersonid_pub_ = nh->advertise<std_msgs::Empty>("activepersonid", 1);
    }

    ~Interaction_manager() {}

   private:
    void semanticscenedescriptionCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("interaction_manager: received message: " << msg);
    }
    void inputCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("interaction_manager: received message: " << msg);
    }
    void dialoguestateCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("interaction_manager: received message: " << msg);
    }
    void robotstateCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("interaction_manager: received message: " << msg);
    }
    void tfCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("interaction_manager: received message: " << msg);
    }
    void hppersonidCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("interaction_manager: received message: " << msg);
    }

    ros::Subscriber semanticscenedescription_sub_;
    ros::Subscriber input_sub_;
    ros::Subscriber dialoguestate_sub_;
    ros::Subscriber robotstate_sub_;
    ros::Subscriber tf_sub_;
    ros::Subscriber hppersonid_sub_;

    ros::Publisher whotolookat_pub_;
    ros::Publisher verbalcommand_pub_;
    ros::Publisher navgoals_pub_;
    ros::Publisher gestures_pub_;
    ros::Publisher activepersonid_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "interaction_manager");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto interaction_manager = Interaction_manager(&private_node_handle_);

    ROS_INFO("Node interaction_manager launched and ready.");
    ros::spin();
    return 0;
}
