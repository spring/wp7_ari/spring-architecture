/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "nav_msgs/OccupancyGrid.h"

class OccupancyMap {
   public:
    OccupancyMap(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        tfbodies_sub_ = nh->subscribe("tfbodies", 1, &OccupancyMap::tfbodiesCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        dense3dmap_sub_ = nh->subscribe("dense3dmap", 1, &OccupancyMap::dense3dmapCallback, this);

        map_refined_pub_ = nh->advertise<nav_msgs::OccupancyGrid>("map_refined", 1);
    }

    ~OccupancyMap() {}

   private:
    void tfbodiesCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("occupancymap: received message: " << msg);
    }
    void dense3dmapCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("occupancymap: received message: " << msg);
    }

    ros::Subscriber tfbodies_sub_;
    ros::Subscriber dense3dmap_sub_;

    ros::Publisher map_refined_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "occupancymap");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto occupancymap = OccupancyMap(&private_node_handle_);

    ROS_INFO("Node occupancymap launched and ready.");
    ros::spin();
    return 0;
}
