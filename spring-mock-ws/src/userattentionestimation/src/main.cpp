/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "tf/transform_broadcaster.h"
#include "hri_msgs/RegionOfInterestStamped.h"

class UserAttentionEstimation {
   public:
    UserAttentionEstimation(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        tffaces_sub_ = nh->subscribe("tffaces", 1, &UserAttentionEstimation::tffacesCallback, this);
        face_roi_sub_ = nh->subscribe("face_roi", 1, &UserAttentionEstimation::face_roiCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        xyattentionheatmap_pub_ = nh->advertise<std_msgs::Empty>("xyattentionheatmap", 1);
    }

    ~UserAttentionEstimation() {}

   private:
    void tffacesCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("userattentionestimation: received message: " << msg);
    }
    void face_roiCallback(const hri_msgs::RegionOfInterestStamped::ConstPtr& msg)
    {
        ROS_INFO_STREAM("userattentionestimation: received message: " << msg);
    }

    ros::Subscriber tffaces_sub_;
    ros::Subscriber face_roi_sub_;

    ros::Publisher xyattentionheatmap_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "userattentionestimation");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto userattentionestimation = UserAttentionEstimation(&private_node_handle_);

    ROS_INFO("Node userattentionestimation launched and ready.");
    ros::spin();
    return 0;
}
