/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "hri_msgs/GazesStamped.h"

class UserVisualFocus {
   public:
    UserVisualFocus(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        scene_sub_ = nh->subscribe("scene", 1, &UserVisualFocus::sceneCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        gazedirection_sub_ = nh->subscribe("gazedirection", 1, &UserVisualFocus::gazedirectionCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        attention_sub_ = nh->subscribe("attention", 1, &UserVisualFocus::attentionCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        depth_sub_ = nh->subscribe("depth", 1, &UserVisualFocus::depthCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        whoslookingatwhat_pub_ = nh->advertise<std_msgs::Empty>("whoslookingatwhat", 1);
        gaze_pub_ = nh->advertise<hri_msgs::GazesStamped>("gaze", 1);
    }

    ~UserVisualFocus() {}

   private:
    void sceneCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("uservisualfocus: received message: " << msg);
    }
    void gazedirectionCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("uservisualfocus: received message: " << msg);
    }
    void attentionCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("uservisualfocus: received message: " << msg);
    }
    void depthCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("uservisualfocus: received message: " << msg);
    }

    ros::Subscriber scene_sub_;
    ros::Subscriber gazedirection_sub_;
    ros::Subscriber attention_sub_;
    ros::Subscriber depth_sub_;

    ros::Publisher whoslookingatwhat_pub_;
    ros::Publisher gaze_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "uservisualfocus");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto uservisualfocus = UserVisualFocus(&private_node_handle_);

    ROS_INFO("Node uservisualfocus launched and ready.");
    ros::spin();
    return 0;
}
