/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "tf/transform_broadcaster.h"
#include "sensor_msgs/Image.h"
#include "hri_msgs/RegionOfInterestStamped.h"
#include "hri_msgs/Skeleton2D.h"

class Hri_fullbody {
   public:
    Hri_fullbody(ros::NodeHandle* nh) {

        image_raw_sub_ = nh->subscribe("image_raw", 1, &Hri_fullbody::image_rawCallback, this);
        body_roi_sub_ = nh->subscribe("body_roi", 1, &Hri_fullbody::body_roiCallback, this);

        face_roi_pub_ = nh->advertise<hri_msgs::RegionOfInterestStamped>("face_roi", 1);
        body_skeleton2d_pub_ = nh->advertise<hri_msgs::Skeleton2D>("body_skeleton2d", 1);
    }

    ~Hri_fullbody() {}

   private:
    void image_rawCallback(const sensor_msgs::Image::ConstPtr& msg)
    {
        ROS_INFO_STREAM("hri_fullbody: received message: " << msg);
    }
    void body_roiCallback(const hri_msgs::RegionOfInterestStamped::ConstPtr& msg)
    {
        ROS_INFO_STREAM("hri_fullbody: received message: " << msg);
    }

    ros::Subscriber image_raw_sub_;
    ros::Subscriber body_roi_sub_;

    ros::Publisher face_roi_pub_;
    ros::Publisher body_skeleton2d_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "hri_fullbody");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto hri_fullbody = Hri_fullbody(&private_node_handle_);

    ROS_INFO("Node hri_fullbody launched and ready.");
    ros::spin();
    return 0;
}
