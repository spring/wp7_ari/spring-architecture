/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"

class Plan_actions {
   public:
    Plan_actions(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        demographics_sub_ = nh->subscribe("demographics", 1, &Plan_actions::demographicsCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        dialoguestate_sub_ = nh->subscribe("dialoguestate", 1, &Plan_actions::dialoguestateCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        tfpersons_sub_ = nh->subscribe("tfpersons", 1, &Plan_actions::tfpersonsCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        semanticscenedescription_sub_ = nh->subscribe("semanticscenedescription", 1, &Plan_actions::semanticscenedescriptionCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        output_pub_ = nh->advertise<std_msgs::Empty>("output", 1);
        // ATTENTION: this topic is not defined in the architecture design
        navgoals_pub_ = nh->advertise<std_msgs::Empty>("navgoals", 1);
        // ATTENTION: this topic is not defined in the architecture design
        interactionstate_pub_ = nh->advertise<std_msgs::Empty>("interactionstate", 1);
    }

    ~Plan_actions() {}

   private:
    void demographicsCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("plan_actions: received message: " << msg);
    }
    void dialoguestateCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("plan_actions: received message: " << msg);
    }
    void tfpersonsCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("plan_actions: received message: " << msg);
    }
    void semanticscenedescriptionCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("plan_actions: received message: " << msg);
    }

    ros::Subscriber demographics_sub_;
    ros::Subscriber dialoguestate_sub_;
    ros::Subscriber tfpersons_sub_;
    ros::Subscriber semanticscenedescription_sub_;

    ros::Publisher output_pub_;
    ros::Publisher navgoals_pub_;
    ros::Publisher interactionstate_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "plan_actions");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto plan_actions = Plan_actions(&private_node_handle_);

    ROS_INFO("Node plan_actions launched and ready.");
    ros::spin();
    return 0;
}
