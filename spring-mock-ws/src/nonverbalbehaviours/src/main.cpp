/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "hri_msgs/Expression.h"
#include "hri_msgs/RegionOfInterestStamped.h"
#include "audio_common_msgs/AudioData.h"

class NonverbalBehaviours {
   public:
    NonverbalBehaviours(ros::NodeHandle* nh) {

        face_roi_sub_ = nh->subscribe("face_roi", 1, &NonverbalBehaviours::face_roiCallback, this);
        voice_audio_sub_ = nh->subscribe("voice_audio", 1, &NonverbalBehaviours::voice_audioCallback, this);

        face_expression_pub_ = nh->advertise<hri_msgs::Expression>("face_expression", 1);
    }

    ~NonverbalBehaviours() {}

   private:
    void face_roiCallback(const hri_msgs::RegionOfInterestStamped::ConstPtr& msg)
    {
        ROS_INFO_STREAM("nonverbalbehaviours: received message: " << msg);
    }
    void voice_audioCallback(const audio_common_msgs::AudioData::ConstPtr& msg)
    {
        ROS_INFO_STREAM("nonverbalbehaviours: received message: " << msg);
    }

    ros::Subscriber face_roi_sub_;
    ros::Subscriber voice_audio_sub_;

    ros::Publisher face_expression_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "nonverbalbehaviours");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto nonverbalbehaviours = NonverbalBehaviours(&private_node_handle_);

    ROS_INFO("Node nonverbalbehaviours launched and ready.");
    ros::spin();
    return 0;
}
