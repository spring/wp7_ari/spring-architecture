/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/String.h"
#include "audio_common_msgs/AudioData.h"

class VoiceSpeechMatching {
   public:
    VoiceSpeechMatching(ros::NodeHandle* nh) {

        speech_streams_sub_ = nh->subscribe("speech_streams", 1, &VoiceSpeechMatching::speech_streamsCallback, this);
        voice_audio_sub_ = nh->subscribe("voice_audio", 1, &VoiceSpeechMatching::voice_audioCallback, this);

        voice_speech_pub_ = nh->advertise<std_msgs::String>("voice_speech", 1);
    }

    ~VoiceSpeechMatching() {}

   private:
    void speech_streamsCallback(const std_msgs::String::ConstPtr& msg)
    {
        ROS_INFO_STREAM("voicespeechmatching: received message: " << msg);
    }
    void voice_audioCallback(const audio_common_msgs::AudioData::ConstPtr& msg)
    {
        ROS_INFO_STREAM("voicespeechmatching: received message: " << msg);
    }

    ros::Subscriber speech_streams_sub_;
    ros::Subscriber voice_audio_sub_;

    ros::Publisher voice_speech_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "voicespeechmatching");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto voicespeechmatching = VoiceSpeechMatching(&private_node_handle_);

    ROS_INFO("Node voicespeechmatching launched and ready.");
    ros::spin();
    return 0;
}
