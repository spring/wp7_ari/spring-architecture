/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "tf/transform_broadcaster.h"
#include "sensor_msgs/Image.h"

class VisualSlam3dMap {
   public:
    VisualSlam3dMap(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        localisationprior_sub_ = nh->subscribe("localisationprior", 1, &VisualSlam3dMap::localisationpriorCallback, this);
        image_torso_sub_ = nh->subscribe("image_torso", 1, &VisualSlam3dMap::image_torsoCallback, this);
        image_head_sub_ = nh->subscribe("image_head", 1, &VisualSlam3dMap::image_headCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        dense3dmap_pub_ = nh->advertise<std_msgs::Empty>("dense3dmap", 1);
    }

    ~VisualSlam3dMap() {}

   private:
    void localisationpriorCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("visualslam3dmap: received message: " << msg);
    }
    void image_torsoCallback(const sensor_msgs::Image::ConstPtr& msg)
    {
        ROS_INFO_STREAM("visualslam3dmap: received message: " << msg);
    }
    void image_headCallback(const sensor_msgs::Image::ConstPtr& msg)
    {
        ROS_INFO_STREAM("visualslam3dmap: received message: " << msg);
    }

    ros::Subscriber localisationprior_sub_;
    ros::Subscriber image_torso_sub_;
    ros::Subscriber image_head_sub_;

    ros::Publisher dense3dmap_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "visualslam3dmap");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto visualslam3dmap = VisualSlam3dMap(&private_node_handle_);

    ROS_INFO("Node visualslam3dmap launched and ready.");
    ros::spin();
    return 0;
}
