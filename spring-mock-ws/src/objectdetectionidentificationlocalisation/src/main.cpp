/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "spring_msgs/DetectedObjectArray.h"
#include "sensor_msgs/Image.h"

class ObjectDetectionidentificationlocalisation {
   public:
    ObjectDetectionidentificationlocalisation(ros::NodeHandle* nh) {

        image_raw_sub_ = nh->subscribe("image_raw", 1, &ObjectDetectionidentificationlocalisation::image_rawCallback, this);

        detected_objects_pub_ = nh->advertise<spring_msgs::DetectedObjectArray>("detected_objects", 1);
    }

    ~ObjectDetectionidentificationlocalisation() {}

   private:
    void image_rawCallback(const sensor_msgs::Image::ConstPtr& msg)
    {
        ROS_INFO_STREAM("objectdetectionidentificationlocalisation: received message: " << msg);
    }

    ros::Subscriber image_raw_sub_;

    ros::Publisher detected_objects_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "objectdetectionidentificationlocalisation");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto objectdetectionidentificationlocalisation = ObjectDetectionidentificationlocalisation(&private_node_handle_);

    ROS_INFO("Node objectdetectionidentificationlocalisation launched and ready.");
    ros::spin();
    return 0;
}
