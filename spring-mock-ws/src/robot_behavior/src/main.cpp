/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "hri_msgs/GroupsStamped.h"
#include "std_msgs/Empty.h"

class Robot_behavior {
   public:
    Robot_behavior(ros::NodeHandle* nh) {

        groups_sub_ = nh->subscribe("groups", 1, &Robot_behavior::groupsCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        status_sub_ = nh->subscribe("status", 1, &Robot_behavior::statusCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        lookat_sub_ = nh->subscribe("lookat", 1, &Robot_behavior::lookatCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        followingnavgoals_sub_ = nh->subscribe("followingnavgoals", 1, &Robot_behavior::followingnavgoalsCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        occupancymap_sub_ = nh->subscribe("occupancymap", 1, &Robot_behavior::occupancymapCallback, this);

        // ATTENTION: this topic is not defined in the architecture design
        lowlevelactions_pub_ = nh->advertise<std_msgs::Empty>("lowlevelactions", 1);
        // ATTENTION: this topic is not defined in the architecture design
        status_pub_ = nh->advertise<std_msgs::Empty>("status", 1);
    }

    ~Robot_behavior() {}

   private:
    void groupsCallback(const hri_msgs::GroupsStamped::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robot_behavior: received message: " << msg);
    }
    void statusCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robot_behavior: received message: " << msg);
    }
    void lookatCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robot_behavior: received message: " << msg);
    }
    void followingnavgoalsCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robot_behavior: received message: " << msg);
    }
    void occupancymapCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robot_behavior: received message: " << msg);
    }

    ros::Subscriber groups_sub_;
    ros::Subscriber status_sub_;
    ros::Subscriber lookat_sub_;
    ros::Subscriber followingnavgoals_sub_;
    ros::Subscriber occupancymap_sub_;

    ros::Publisher lowlevelactions_pub_;
    ros::Publisher status_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "robot_behavior");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto robot_behavior = Robot_behavior(&private_node_handle_);

    ROS_INFO("Node robot_behavior launched and ready.");
    ros::spin();
    return 0;
}
