/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/String.h"
#include "tf/transform_broadcaster.h"
#include "hri_msgs/AgeAndGender.h"
#include "std_msgs/Empty.h"

class PersonManager {
   public:
    PersonManager(ros::NodeHandle* nh) {

        face_demographics_sub_ = nh->subscribe("face_demographics", 1, &PersonManager::face_demographicsCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        tffaces_sub_ = nh->subscribe("tffaces", 1, &PersonManager::tffacesCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        candidate_matchshri_msgsidsmatch_sub_ = nh->subscribe("candidate_matchshri_msgsidsmatch", 1, &PersonManager::candidate_matchshri_msgsidsmatchCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        tfvoices_sub_ = nh->subscribe("tfvoices", 1, &PersonManager::tfvoicesCallback, this);

        person_face_id_pub_ = nh->advertise<std_msgs::String>("person_face_id", 1);
        person_demographics_pub_ = nh->advertise<hri_msgs::AgeAndGender>("person_demographics", 1);
        body_id_pub_ = nh->advertise<std_msgs::String>("body_id", 1);
        person_voice_id_pub_ = nh->advertise<std_msgs::String>("person_voice_id", 1);
    }

    ~PersonManager() {}

   private:
    void face_demographicsCallback(const hri_msgs::AgeAndGender::ConstPtr& msg)
    {
        ROS_INFO_STREAM("personmanager: received message: " << msg);
    }
    void tffacesCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("personmanager: received message: " << msg);
    }
    void candidate_matchshri_msgsidsmatchCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("personmanager: received message: " << msg);
    }
    void tfvoicesCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("personmanager: received message: " << msg);
    }

    ros::Subscriber face_demographics_sub_;
    ros::Subscriber tffaces_sub_;
    ros::Subscriber candidate_matchshri_msgsidsmatch_sub_;
    ros::Subscriber tfvoices_sub_;

    ros::Publisher person_face_id_pub_;
    ros::Publisher person_demographics_pub_;
    ros::Publisher body_id_pub_;
    ros::Publisher person_voice_id_pub_;

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "personmanager");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto personmanager = PersonManager(&private_node_handle_);

    ROS_INFO("Node personmanager launched and ready.");
    ros::spin();
    return 0;
}
