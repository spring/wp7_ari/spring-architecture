/*
 Copyright 2021-2025, SPRING Consortium

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "std_msgs/String.h"

class RobotGui {
   public:
    RobotGui(ros::NodeHandle* nh) {

        // ATTENTION: this topic is not defined in the architecture design
        additionalsupportmaterial_sub_ = nh->subscribe("additionalsupportmaterial", 1, &RobotGui::additionalsupportmaterialCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        ttsfeedback_sub_ = nh->subscribe("ttsfeedback", 1, &RobotGui::ttsfeedbackCallback, this);
        // ATTENTION: this topic is not defined in the architecture design
        speechoutput_sub_ = nh->subscribe("speechoutput", 1, &RobotGui::speechoutputCallback, this);
        voice_speech_sub_ = nh->subscribe("voice_speech", 1, &RobotGui::voice_speechCallback, this);

    }

    ~RobotGui() {}

   private:
    void additionalsupportmaterialCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robotgui: received message: " << msg);
    }
    void ttsfeedbackCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robotgui: received message: " << msg);
    }
    void speechoutputCallback(const std_msgs::Empty::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robotgui: received message: " << msg);
    }
    void voice_speechCallback(const std_msgs::String::ConstPtr& msg)
    {
        ROS_INFO_STREAM("robotgui: received message: " << msg);
    }

    ros::Subscriber additionalsupportmaterial_sub_;
    ros::Subscriber ttsfeedback_sub_;
    ros::Subscriber speechoutput_sub_;
    ros::Subscriber voice_speech_sub_;


};

int main(int argc, char** argv) {
    ros::init(argc, argv, "robotgui");

    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be
    // run simultaneously while using different parameters.
    ros::NodeHandle private_node_handle_("~");

    auto robotgui = RobotGui(&private_node_handle_);

    ROS_INFO("Node robotgui launched and ready.");
    ros::spin();
    return 0;
}
