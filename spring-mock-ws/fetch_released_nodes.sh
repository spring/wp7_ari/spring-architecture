#! /usr/bin/sh

# This script clone all the SPRING ROS nodes that are marked as released in the
# architecture design.


NODE_PATH=src/hri_msgs
if [ -d "$NODE_PATH" ]; then
    rm -rf $NODE_PATH;
fi
cd $NODE_PATH
echo "Cloning  hri_msgs (ROS node: hri_msgs), branch: 0.1.1 to src/ ..."
git clone --depth 1 --branch 0.1.1  git@gitlab:ros4hri/hri_msgs.git src/hri_msgs

NODE_PATH=src/respeaker_ros
if [ -d "$NODE_PATH" ]; then
    rm -rf $NODE_PATH;
fi
cd $NODE_PATH
echo "Cloning respeaker_ros (ROS node: respeaker_ros), branch: master to src/ ..."
git clone --depth 1 --branch master git@gitlab.inria.fr:spring/wp7_ari/respeaker_ros.git src/respeaker_ros

NODE_PATH=src/audio_processing
if [ -d "$NODE_PATH" ]; then
    rm -rf $NODE_PATH;
fi
cd $NODE_PATH
echo "Cloning audio_processing (ROS node: audio_processing), branch: BIU_dev to /tmp ..."
rm -rf /tmp/audio_processing
git clone --depth 1 --branch BIU_dev https://gitlab.inria.fr/spring/wp5_spoken_conversations/asr /tmp/audio_processing
echo "Copying subfolder audio_processing  to src/ ..."
cp -R /tmp/audio_processing/audio_processing  src/audio_processing

NODE_PATH=src/google_asr
if [ -d "$NODE_PATH" ]; then
    rm -rf $NODE_PATH;
fi
cd $NODE_PATH
echo "Cloning google_asr (ROS node: google_asr), branch: BIU_dev to /tmp ..."
rm -rf /tmp/google_asr
git clone --depth 1 --branch BIU_dev https://gitlab.inria.fr/spring/wp5_spoken_conversations/asr /tmp/google_asr
echo "Copying subfolder google_asr/google_asr to src/ ..."
cp -R /tmp/google_asr/google_asr/google_asr src/google_asr

NODE_PATH=src/spring_msgs
if [ -d "$NODE_PATH" ]; then
    rm -rf $NODE_PATH;
fi
cd $NODE_PATH
echo "Cloning  spring_msgs (ROS node: spring_msgs), branch: 0.0.2 to src/ ..."
git clone --depth 1 --branch 0.0.2 git@gitlab.inria.fr:spring/wp7_ari/spring_msgs.git src/spring_msgs

