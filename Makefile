
SRC=architecture.json

BOXOLOGY ?= boxology

all: docx

%.tex: %.json
	$(BOXOLOGY) -t $(<) > $(@)

%.md: %.json
	$(BOXOLOGY) -m $(<)

%.svg: %.tex
	latex $(<)
	dvisvgm $(<:.tex=.dvi)

%.pdf: %.tex
	pdflatex $(<)

%.docx: %.md
	pandoc spring-architecture.md --toc --toc-depth=2 --reference-doc=spring-template.docx -o $(@)
	@echo "\n$(@) is ready."

docx: $(SRC:.json=.docx)

%.odt: %.md
	pandoc spring-architecture.md --toc --toc-depth=2 --reference-doc=spring-template.odt -o $(@)
	@echo "\n$(@) is ready."

odt: $(SRC:.json=.odt)


pdf: $(SRC:.json=.pdf)

svg: $(SRC:.json=.svg)

md: $(SRC:.json=.md)

clean:
	rm -f $(SRC:.json=.pdf) $(SRC:.json=.md) $(SRC:.json=.docx) $(SRC:.json=.svg) $(SRC:.json=.tex)

