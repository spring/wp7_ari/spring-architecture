---
title: "SPRING architecture -- version 2.2.0"
subtitle: "EU H2020 SPRING -- D7.5: Final Software Architecture"
author: Séverin Lemaignan
---


# Overview of modules

| **Node** | **Partner** | **Status** | **Description** |
|----------|-------------|------------|-----------------|
| [riva_asr](#riva_asr) | BIU | released | Speech recognition based on NVIDIA Riva (2nd instance for 2nd speaker). Code: [https://gitlab.inria.fr/spring/wp3_av_perception/riva_asr_ros_client ](https://gitlab.inria.fr/spring/wp3_av_perception/riva_asr_ros_client )|
| [speaker_separation](#speaker_separation) | BIU | released | blind separation + VAD + enhancement. Code: [ https://gitlab.inria.fr/spring/wp3_av_perception/audio_separation.git]( https://gitlab.inria.fr/spring/wp3_av_perception/audio_separation.git)|
| [CSD](#csd) | BIU | not yet implemented | |
| [speaker_extraction](#speaker_extraction) | BIU | released |  Voice embedding + speaker separation together. Code: [https://gitlab.inria.fr/spring/wp3_av_perception/1ch_speaker_extraction ](https://gitlab.inria.fr/spring/wp3_av_perception/1ch_speaker_extraction )|
| [single_speaker_noise_reduction](#single_speaker_noise_reduction) | BIU | released | Audio pre-processing (incl. noise cancellation). Code: [https://gitlab.inria.fr/spring/wp3_av_perception/speech-enhancement SUBFOLDER:audio_processing ](https://gitlab.inria.fr/spring/wp3_av_perception/speech-enhancement SUBFOLDER:audio_processing )|
| [speakers_id_and_DOA](#speakers_id_and_doa) | BIU | released | Speaker identification based on voice embeddings. Code: [https://gitlab.inria.fr/spring/wp4_behavior/non-integrated-contributions/speaker_identification](https://gitlab.inria.fr/spring/wp4_behavior/non-integrated-contributions/speaker_identification)|
| [audio_arbitrer](#audio_arbitrer) | BIU | released | Assigns voices and audio stream, and publishes ROS4HRI-compatible ROS messages. Code: [https://gitlab.inria.fr/spring/wp3_av_perception/audio_manager ](https://gitlab.inria.fr/spring/wp3_av_perception/audio_manager )|
| [slam_rtabmap](#slam_rtabmap) | CVUT | released |  RTABmap based SLAM. Code: [https://gitlab.inria.fr/spring/wp2_mapping_localization/rtabmap-orbslam2](https://gitlab.inria.fr/spring/wp2_mapping_localization/rtabmap-orbslam2)|
| [HLoc](#hloc) | CVUT | released | Global localization, service-based. Code: [https://gitlab.inria.fr/spring/wp2_mapping_localization/hloc-mapping-localization.git](https://gitlab.inria.fr/spring/wp2_mapping_localization/hloc-mapping-localization.git)|
| [Yolact3D](#yolact3d) | CVUT | released | Object detection/identification/localisation. Code: [ https://gitlab.inria.fr/spring/wp2_mapping_localization/yolact3d.git]( https://gitlab.inria.fr/spring/wp2_mapping_localization/yolact3d.git)|
| [experiment_exporter](#experiment_exporter) | ERM | released |  Experiment_exporter is in charge of logging the experiment data.. Code: [https://gitlab.inria.fr/spring/wp1_user_application/export-dialog.git](https://gitlab.inria.fr/spring/wp1_user_application/export-dialog.git)|
| [robot gui](#robotgui) | ERM | released |  Robot tablet interface. Code: [https://gitlab.inria.fr/spring/wp1_user_application/user_application.git](https://gitlab.inria.fr/spring/wp1_user_application/user_application.git)|
| [experimenter_gui](#experimenter_gui) | ERM | released |  Web server for the experimenter tablet. Code: [https://gitlab.inria.fr/spring/wp1_user_application/exp-gui.git](https://gitlab.inria.fr/spring/wp1_user_application/exp-gui.git)|
| [dialogue_say](#dialogue_say) | HWU | released |  proxy to robot's TTS action server for ERM convenience. Code: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_say](https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_say)|
| [interaction_manager](#interaction_manager) | HWU | released | . Code: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/interaction.git SUBFOLDER:interaction_manager](https://gitlab.inria.fr/spring/wp5_spoken_conversations/interaction.git SUBFOLDER:interaction_manager)|
| [ros_petri_net_node](#ros_petri_net_node) | HWU | released | Petrinet-based task planning. Code: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/ros_petri_net_planner](https://gitlab.inria.fr/spring/wp5_spoken_conversations/ros_petri_net_planner)|
| [dialogue_speech](#dialogue_speech) | HWU | released | Speech pre-processing (incl. end of speech detection). Code: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_speech](https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_speech)|
| [nlp_node](#nlp_node) | HWU | released | ALANA chatbot|
| [dialogue_arbitrer](#dialogue_arbitrer) | HWU | released | Dialogue arbitrer. Code: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_arbiter](https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_arbiter)|
| [recipe_planner](#recipe_planner) | HWU | released | . Code: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/plan_actions.git](https://gitlab.inria.fr/spring/wp5_spoken_conversations/plan_actions.git)|
| [social_state_analyzer](#social_state_analyzer) | HWU | released | Social scene understanding|
| [social_strategy_supervisor](#social_strategy_supervisor) | HWU | released | High-level interaction supervisor|
| [go_to_group_action_server](#go_to_group_action_server) | INRIA | released | Robot action server (group approach)|
| [go_to_person_action_server](#go_to_person_action_server) | INRIA | released | Robot action server (person approach)|
| [occupancy_map_republisher](#occupancy_map_republisher) | INRIA | not yet implemented | |
| [front_fisheye_2d_body_pose_detector_op](#front_fisheye_2d_body_pose_detector_op) | INRIA | released | 2D skeleton estimator This node estimates the 2.5D (x,y,theta) pose of nearby persons.. Code: [https://gitlab.inria.fr/spring/wp3_av_perception/front_fisheye_2d_body_pose_detector](https://gitlab.inria.fr/spring/wp3_av_perception/front_fisheye_2d_body_pose_detector)|
| [group_detector](#group_detector) | INRIA | released | Group detection (incl. f-formations). Code: [https://gitlab.inria.fr/spring/wp4_behavior/group_detector](https://gitlab.inria.fr/spring/wp4_behavior/group_detector)|
| [body_to_face_mapper](#body_to_face_mapper) | INRIA | released | Face-body matching. Code: [node associates detected bodies to detected faces in image-space. REPO:https://gitlab.inria.fr/spring/wp3_av_perception/body_to_face_mapper](node associates detected bodies to detected faces in image-space. REPO:https://gitlab.inria.fr/spring/wp3_av_perception/body_to_face_mapper)|
| [basestation_republisher](#basestation_republisher) | INRIA | released | Node to republish compressed video streams on the SPRING basestation. Code: [https://gitlab.inria.fr/spring/wp3_av_perception/docker_republish](https://gitlab.inria.fr/spring/wp3_av_perception/docker_republish)|
| [go_to_body_action_server](#go_to_body_action_server) | INRIA | released | Robot action server (body approach)|
| [go_to_position_action_server](#go_to_position_action_server) | INRIA | released | Robot action server (navigation to location)|
| [look_at_person_server](#look_at_person_server) | INRIA | released | Robot action server ('look at person' server)|
| [look_at_action_server](#look_at_action_server) | INRIA | released | Robot action server (generic 'look at' action)|
| [social_mpc](#social_mpc) | INRIA | released | . Code: [ https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior]( https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior)|
| [look_at_position_action_server](#look_at_position_action_server) | INRIA | released | Robot action server ('look at location' action) |
| [pointcloud2occmap](#pointcloud2occmap) | INRIA | not yet implemented | |
| [front_fisheye_2d_body_tracker](#front_fisheye_2d_body_tracker) | INRIA | released | 2D body detector and tracker, based on the FairMOT algorithm.. Code: [ https://gitlab.inria.fr/spring/wp3_av_perception/multi-person_visual_tracker/]( https://gitlab.inria.fr/spring/wp3_av_perception/multi-person_visual_tracker/)|
| [voicebodycandidatematches](#voicebodycandidatematches) | INRIA | released | Matching between localised voices and detected bodies|
| [body_3d_tracker](#body_3d_tracker) | INRIA | released | Laser-based 3D people pose estimation REPO:https://gitlab.inria.fr/spring/wp3_av_perception/body_3d_tracker|
| [respeaker_ros](#respeaker_ros) | PAL | released | Microphone array driver REPO:https://gitlab.inria.fr/spring/wp7_ari/respeaker_ros.git BIN:respeaker_multichan_node.py|
| [fisheye](#fisheye) | PAL | released |  frontal fisheye camera driver|
| [raspicam](#raspicam) | PAL | released |  RGB head camera driver|
| [Robot functional layer](#robotfunctionallayer) | PAL | released |  robot's hardware interfaces|
| [hri_person_manager](#hri_person_manager) | PAL | released | Probabilistic fusion of faces, bodies, voices into persons. Code: [https://gitlab.inria.fr/spring/wp7_ari/hri_person_manager.git](https://gitlab.inria.fr/spring/wp7_ari/hri_person_manager.git)|
| [torso_rgbd_camera](#torso_rgbd_camera) | PAL | released |  Frontal RGB-D camera driver|
| [people_facts](#people_facts) | PAL | released | Semantic bridge between human perception and the knowledge base. Code: [https://gitlab.inria.fr/spring/wp7_ari/people_facts](https://gitlab.inria.fr/spring/wp7_ari/people_facts)|
| [knowledge_core](#knowledge_core) | PAL | released | Robot's RDF/OWL knowledge base. Code: [https://gitlab.inria.fr/spring/wp7_ari/knowledge_core](https://gitlab.inria.fr/spring/wp7_ari/knowledge_core)|
| [soft_biometrics_estimator](#soft_biometrics_estimator) | UNITN | released | Detects age and gender. Code: [https://gitlab.inria.fr/spring/wp4_behavior/wp4_behavior_understanding SUBFOLDER:wp4_people_characteristics BIN:soft_biometrics_estimator.py](https://gitlab.inria.fr/spring/wp4_behavior/wp4_behavior_understanding SUBFOLDER:wp4_people_characteristics BIN:soft_biometrics_estimator.py)|
| [emotion_estimation](#emotion_estimation) | UNITN | released | Non-verbal behaviour generation|
| [face_tracker](#face_tracker) | UNITN | released |  face detection and tracking. Code: [ https://gitlab.inria.fr/spring/wp4_behavior/face-tracker]( https://gitlab.inria.fr/spring/wp4_behavior/face-tracker)|
| [mask_detector](#mask_detector) | UNITN | released | Detects presence of a facial mask. Code: [https://gitlab.inria.fr/spring/wp4_behavior/mask-detection BIN:mask_detector.py](https://gitlab.inria.fr/spring/wp4_behavior/mask-detection BIN:mask_detector.py)|
| [depth_estimation](#depth_estimation) | UNITN | released | Monocular depth estimation. Code: [https://gitlab.inria.fr/spring/wp4_behavior/depth-estimation](https://gitlab.inria.fr/spring/wp4_behavior/depth-estimation)|
| [human_2d_pose_estimation](#human_2d_pose_estimation) | UNITN | released | 2D skeleton extractor. Code: [https://gitlab.inria.fr/spring/wp4_behavior/human-2d-pose-estimation](https://gitlab.inria.fr/spring/wp4_behavior/human-2d-pose-estimation)|
| [gaze_estimation](#gaze_estimation) | UNITN | released | Monocular gaze estimation on planar image This node uses deep learning to estimate, on a given frame, the focus of attention of a detected face. It outputs the 2D coordinate of the most likely focus of attention, in the image space. REPO:https://gitlab.inria.fr/spring/wp4_behavior/gaze-estimation|
| [activity_recognition](#activity_recognition) | UNITN | released | Activity recognition|

# Detailed description




## BIU



### audio_arbitrer {#audio_arbitrer}



*The node audio_arbitrer (id: `audio_arbitrer`) is maintained by BIU.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: [https://gitlab.inria.fr/spring/wp3_av_perception/audio_manager ](https://gitlab.inria.fr/spring/wp3_av_perception/audio_manager )


#### Inputs

 - Input: count_active_speakers
 - Input: processed_audio
 - Input: active_voices

#### Outputs

 - Output: `tf: /voice_*` (tf) 
 - Topic publication: `/humans/voices/<id>/speech [hri_msgs/LiveSpeech]`

 - Topic publication: `/humans/voices/<id>/doa [std_msgs/Float32]`


#### Dependencies

- `std_msgs/Empty`
- `tf/transform_broadcaster`
- `hri_msgs/LiveSpeech`
- `std_msgs/Float32`




### CSD {#csd}



*The node CSD (id: `csd`) is maintained by BIU.*

#### Status

This node is not yet implemented.


#### Inputs

 - Input: /audio/raw_audio

#### Outputs

 - Output: count_active_speakers

#### Dependencies

- `std_msgs/Empty`




### riva_asr {#riva_asr}



*The node riva_asr (id: `riva_asr`) is maintained by BIU.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp3_av_perception/riva_asr_ros_client ](https://gitlab.inria.fr/spring/wp3_av_perception/riva_asr_ros_client )


#### Inputs

 - Input: audio

#### Outputs

 - Output: text

#### Dependencies

- `std_msgs/Empty`




### single_speaker_noise_reduction {#single_speaker_noise_reduction}

 This node performs: - speech echo cancelation, - single microphone audio enhancement Used for the single-speaker only pipeline.

*The node single_speaker_noise_reduction (id: `single_speaker_noise_reduction`) is maintained by BIU.*

#### Status

Implemented. Current release/branch: BIU_dev

Source code repository: [https://gitlab.inria.fr/spring/wp3_av_perception/speech-enhancement SUBFOLDER:audio_processing ](https://gitlab.inria.fr/spring/wp3_av_perception/speech-enhancement SUBFOLDER:audio_processing )


#### Inputs

 - Input: /audio/raw_audio 

#### Outputs

 - Topic publication: `/audio/enh_audio [spring_msgs/RawAudioData]`


#### Dependencies

- `std_msgs/Empty`
- `spring_msgs/RawAudioData`




### speaker_extraction {#speaker_extraction}



*The node speaker_extraction (id: `speaker_extraction`) is maintained by BIU.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: [https://gitlab.inria.fr/spring/wp3_av_perception/1ch_speaker_extraction ](https://gitlab.inria.fr/spring/wp3_av_perception/1ch_speaker_extraction )


#### Inputs

 - Input: streams

#### Outputs

 - Topic publication: `/audio/voice_stream* [audio_common_msgs/AudioData]`


#### Dependencies

- `std_msgs/Empty`
- `audio_common_msgs/AudioData`




### speaker_separation {#speaker_separation}



*The node speaker_separation (id: `speaker_separation`) is maintained by BIU.*

#### Status

Implemented. Current release/branch: main

Source code repository: [ https://gitlab.inria.fr/spring/wp3_av_perception/audio_separation.git]( https://gitlab.inria.fr/spring/wp3_av_perception/audio_separation.git)


#### Inputs

 - Input: /audio/raw_audio

#### Outputs

 - Output: streams

#### Dependencies

- `std_msgs/Empty`




### speakers_id_and_DOA {#speakers_id_and_doa}



*The node speakers_id_and_DOA (id: `speakers_id_and_doa`) is maintained by BIU.*

#### Status

Implemented. Current release/branch: dual_speaker_ecapa

Source code repository: [https://gitlab.inria.fr/spring/wp4_behavior/non-integrated-contributions/speaker_identification](https://gitlab.inria.fr/spring/wp4_behavior/non-integrated-contributions/speaker_identification)


#### Inputs

 - Input: /audio/voice_stream*
 - Input: count_active_speakers

#### Outputs

 - Topic publication: `/humans/voices/<id>/doa [std_msgs/Float32]`

 - Topic publication: `/humans/voices/<id>/audio [audio_common_msgs/AudioData]`


#### Dependencies

- `std_msgs/Empty`
- `std_msgs/Float32`
- `audio_common_msgs/AudioData`




## CVUT



### HLoc {#hloc}

 Docker not yet published 

*The node HLoc (id: `hloc`) is maintained by CVUT.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp2_mapping_localization/hloc-mapping-localization.git](https://gitlab.inria.fr/spring/wp2_mapping_localization/hloc-mapping-localization.git)


#### Inputs

 - Input: `tf: camera frames ` (tf) 
 - Topic subscription: `/robot_pose [geometry_msgs/PoseWithCovarianceStamped]`

 - Input: /front_camera/fisheye/image_raw/compressed
 - Input: /rear_camera/fisheye/image_raw/compressed

#### Outputs

 - Output: ROS service: pose + covariance

#### Dependencies

- `tf/transform_listener`
- `std_msgs/Empty`
- `geometry_msgs/PoseWithCovarianceStamped`




### slam_rtabmap {#slam_rtabmap}



*The node slam_rtabmap (id: `slam_rtabmap`) is maintained by CVUT.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: [https://gitlab.inria.fr/spring/wp2_mapping_localization/rtabmap-orbslam2](https://gitlab.inria.fr/spring/wp2_mapping_localization/rtabmap-orbslam2)


#### Inputs

 - Input: torso_front_camera/infra*/*
 - Input: /torso_front_camera/imu
 - Input: [call HLoc to perform global localization]

#### Outputs

 - Output: `tf: /odom` (tf) 
 - Topic publication: `/slam/occupancy_map [OccupancyGrid/OccupancyGrid]`

 - Topic publication: `/robot_pose [geometry_msgs/PoseWithCovarianceStamped]`


#### Dependencies

- `tf/transform_broadcaster`
- `std_msgs/Empty`
- `OccupancyGrid/OccupancyGrid`
- `geometry_msgs/PoseWithCovarianceStamped`




### Yolact3D {#yolact3d}

 Publishes a set of 3D points with the probability distribution of object classes at that point.  ETA: not clear yet. 

*The node Yolact3D (id: `yolact3d`) is maintained by CVUT.*

#### Status

Implemented. Current release/branch: master

Source code repository: [ https://gitlab.inria.fr/spring/wp2_mapping_localization/yolact3d.git]( https://gitlab.inria.fr/spring/wp2_mapping_localization/yolact3d.git)


#### Inputs

 - Topic subscription: `/slam/occupancy_map [OccupancyGrid/OccupancyGrid]`

 - Input: tf
 - Input: /torso_front_camera/aligned_depth_to_color [sensor_msgs/Image
 - Topic subscription: `/torso_front_camera/color/image_raw [sensor_msgs/Image]`


#### Outputs

 - Topic publication: `/yolact3d/detected_objects_distribution [yolact3d/Yolact3DObjects]`


#### Dependencies

- `yolact3d/Yolact3DObjects`
- `OccupancyGrid/OccupancyGrid`
- `std_msgs/Empty`
- `sensor_msgs/Image`




## ERM



### experiment_exporter {#experiment_exporter}



*The node experiment_exporter (id: `experiment_exporter`) is maintained by ERM.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp1_user_application/export-dialog.git](https://gitlab.inria.fr/spring/wp1_user_application/export-dialog.git)


#### Inputs

 - Topic subscription: `/dialogue_say/text [String/String]`

 - Topic subscription: `/experiment_exporter/file_update [String/String]`

 - Topic subscription: `/dialogue_speech/eos [dialogue_msgs/EndOfSpeech]`

 - Topic subscription: `/experiment_exporter/error_feedback [String/String]`


#### Outputs


#### Dependencies

- `String/String`
- `dialogue_msgs/EndOfSpeech`




### experimenter_gui {#experimenter_gui}



*The node experimenter_gui (id: `experimenter_gui`) is maintained by ERM.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp1_user_application/exp-gui.git](https://gitlab.inria.fr/spring/wp1_user_application/exp-gui.git)


#### Inputs

 - Input: /dialogue_start/status
 - Input: /diagnostics
 - Input: /slam/localization_pose

#### Outputs

 - Output: /dialogue_start/goal [dialogue_msgs/StartDialogueActionGoal] 
 - Topic publication: `/dialogue_arbiter/reset [Empty/Empty]`

 - Topic publication: `/interaction_manager/start [Empty/Empty]`

 - Topic publication: `/web/go_to [pal_web_msgs/WebGoTo]`

 - Output: /speech/speed
 - Topic publication: `/experiment_exporter/error_feedback [String/String]`

 - Topic publication: `/experiment_exporter/file_update [String/String]`


#### Dependencies

- `std_msgs/Empty`
- `Empty/Empty`
- `pal_web_msgs/WebGoTo`
- `String/String`




### robot gui {#robotgui}



*The node robot gui (id: `robotgui`) is maintained by ERM.*

#### Status

Implemented. Current release/branch: master

Source code repository: [https://gitlab.inria.fr/spring/wp1_user_application/user_application.git](https://gitlab.inria.fr/spring/wp1_user_application/user_application.git)


#### Inputs

 - Input: /tts/feedback
 - Input: /human_dialogue
 - Input: /audio/is_listening

#### Outputs


#### Dependencies

- `std_msgs/Empty`




## HWU



### dialogue_arbitrer {#dialogue_arbitrer}



*The node dialogue_arbitrer (id: `dialogue_arbitrer`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_arbiter](https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_arbiter)


#### Inputs

 - Topic subscription: `/dialogue_start [action server/action server]`

 - Topic subscription: `/dialogue_arbitrer/end_conv [service/service]`

 - Topic subscription: `/dialogue_speech/eos [dialogue_msgs/EndOfSpeech]`


#### Outputs

 - Topic publication: `/nlp_node/get_answer [service/service]`

 - Output: /dialogue_start/status
 - Output: /dialogue_start/feedback
 - Output: /human_dialogue
 - Topic publication: `/dialogue_say/say [service/service]`

 - Topic publication: `/RPN [action/action]`

 - Topic publication: `/task*_ros_server_action [action/action]`


#### Dependencies

- `service/service`
- `action server/action server`
- `std_msgs/Empty`
- `action/action`
- `dialogue_msgs/EndOfSpeech`




### dialogue_say {#dialogue_say}



*The node dialogue_say (id: `dialogue_say`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_say](https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_say)


#### Inputs

 - Topic subscription: `/dialogue_say/say [service/service]`

 - Input: /speech/speed
 - Input: /tts/status

#### Outputs

 - Topic publication: `/dialogue_say/text [String/String]`

 - Output: /tts/feedback
 - Topic publication: `/tts [action_server/action_server]`


#### Dependencies

- `String/String`
- `service/service`
- `std_msgs/Empty`
- `action_server/action_server`




### dialogue_speech {#dialogue_speech}



*The node dialogue_speech (id: `dialogue_speech`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: master

Source code repository: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_speech](https://gitlab.inria.fr/spring/wp5_spoken_conversations/dialogue SUBFOLDER:dialogue_speech)


#### Inputs

 - Topic subscription: `/humans/voices/<id>/speech [hri_msgs/LiveSpeech]`

 - Topic subscription: `/social_state_analyser/state [std_msgs/String]`


#### Outputs

 - Topic publication: `/dialogue_speech/eos [dialogue_msgs/EndOfSpeech]`


#### Dependencies

- `hri_msgs/LiveSpeech`
- `dialogue_msgs/EndOfSpeech`
- `std_msgs/String`




### interaction_manager {#interaction_manager}



*The node interaction_manager (id: `interaction_manager`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: spring_dev

Source code repository: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/interaction.git SUBFOLDER:interaction_manager](https://gitlab.inria.fr/spring/wp5_spoken_conversations/interaction.git SUBFOLDER:interaction_manager)


#### Inputs

 - Input: TF
 - Topic subscription: `/nlp_node/nlu [JSON String/JSON String]`

 - Input: input
 - Topic subscription: `/register_server [service/service]`

 - Topic subscription: `/interaction_manager/update [service/service]`

 - Topic subscription: `/interaction_manager/query [service/service]`

 - Input: /dialogue_start/feedback
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`

 - Input: semantic scene description [ON HOLD]
 - Input: /dialogue_start/status
 - Topic subscription: `/humans/persons/<id>/softbiometrics [hri_msgs/Softbiometrics]`


#### Outputs

 - Topic publication: `/task*_ros_server_action [action/action]`

 - Output: /dialogue_start
 - Output: /navigation goals
 - Output: /look_at goals
 - Output: gestures
 - Topic publication: `/RPN [action/action]`

 - Output: /social_strategy_supervisor_server/goal

#### Dependencies

- `action/action`
- `std_msgs/Empty`
- `JSON String/JSON String`
- `service/service`
- `ControllerStatus/ControllerStatus`
- `hri_msgs/Softbiometrics`




### nlp_node {#nlp_node}

  

*The node nlp_node (id: `nlp_node`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Topic subscription: `/nlp_node/get_answer [service/service]`


#### Outputs

 - Topic publication: `/nlp_node/nlu [JSON String/JSON String]`


#### Dependencies

- `service/service`
- `JSON String/JSON String`




### recipe_planner {#recipe_planner}



*The node recipe_planner (id: `recipe_planner`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: spring_dev

Source code repository: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/plan_actions.git](https://gitlab.inria.fr/spring/wp5_spoken_conversations/plan_actions.git)


#### Inputs

 - Input: semantic scene description
 - Topic subscription: `/task*_ros_server_action [action/action]`

 - Input: PDDL yaml library
 - Topic subscription: `/humans/persons/<id>/softbiometrics [hri_msgs/Softbiometrics]`


#### Outputs

 - Output: /queries
 - Output: /updates
 - Topic publication: `/RPN [action/action]`

 - Topic publication: `/register_server [service/service]`


#### Dependencies

- `std_msgs/Empty`
- `action/action`
- `service/service`
- `hri_msgs/Softbiometrics`




### ros_petri_net_node {#ros_petri_net_node}



*The node ros_petri_net_node (id: `ros_petri_net_node`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: [https://gitlab.inria.fr/spring/wp5_spoken_conversations/ros_petri_net_planner](https://gitlab.inria.fr/spring/wp5_spoken_conversations/ros_petri_net_planner)


#### Inputs

 - Topic subscription: `/RPN [action server/action server]`


#### Outputs


#### Dependencies

- `action server/action server`




### social_state_analyzer {#social_state_analyzer}

  

*The node social_state_analyzer (id: `social_state_analyzer`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: /h/p/tracked
 - Topic subscription: `/kb/query [service/service]`

 - Input: /h/p/*
 - Input: /h/b/tracked
 - Input: /h/b/*
 - Input: /h/f/*
 - Topic subscription: `/social_state_analyzer_server [action_server/action_server]`

 - Input: /h/f/tracked

#### Outputs

 - Topic publication: `/social_state_analyser/state [std_msgs/String]`


#### Dependencies

- `std_msgs/Empty`
- `service/service`
- `std_msgs/String`
- `action_server/action_server`




### social_strategy_supervisor {#social_strategy_supervisor}

  

*The node social_strategy_supervisor (id: `social_strategy_supervisor`) is maintained by HWU.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Topic subscription: `/social_state_analyser/state [std_msgs/String]`

 - Topic subscription: `/social_strategy_supervisor_server [action_server/action_server]`


#### Outputs

 - Output: /look_at goals
 - Output: /go_to goals

#### Dependencies

- `std_msgs/Empty`
- `std_msgs/String`
- `action_server/action_server`




## INRIA



### basestation_republisher {#basestation_republisher}



*The node basestation_republisher (id: `basestation_republisher`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: master

Source code repository: [https://gitlab.inria.fr/spring/wp3_av_perception/docker_republish](https://gitlab.inria.fr/spring/wp3_av_perception/docker_republish)


#### Inputs

 - Input: /torso_front_camera/aligned_depth_to_color/image_raw/theora
 - Topic subscription: `/front_camera/fisheye/image_raw/compressed [sensor_msgs/CompressedImage]`

 - Input: /torso_front_camera/color/image_raw/theora
 - Input: /head_front_camera/color/image_raw/compressed

#### Outputs

 - Topic publication: `/*_basestation/head_front_camera/... [sensor_msgs/Image]`

 - Topic publication: `/*_basestation/fisheye/... [sensor_msgs/Image]`


#### Dependencies

- `sensor_msgs/Image`
- `std_msgs/Empty`
- `sensor_msgs/CompressedImage`




### body_3d_tracker {#body_3d_tracker}



*The node body_3d_tracker (id: `body_3d_tracker`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: main

Source code repository: []()


#### Inputs

 - Topic subscription: `/humans/bodies/<id>/skeleton2d [hri_msg/Skeleton2D]`

 - Topic subscription: `/front_camera/fisheye/image_raw [sensor_msgs/Image]`

 - Topic subscription: `/tracker/tracker_output [std_msgs/String]`


#### Outputs

 - Output: `tf: /body_*` (tf) 
 - Topic publication: `/humans/bodies/tracked [hri_msgs/IdsList]`


#### Dependencies

- `tf/transform_broadcaster`
- `hri_msg/Skeleton2D`
- `sensor_msgs/Image`
- `std_msgs/String`
- `hri_msgs/IdsList`




### body_to_face_mapper {#body_to_face_mapper}



*The node body_to_face_mapper (id: `body_to_face_mapper`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: main

Source code repository: [node associates detected bodies to detected faces in image-space. REPO:https://gitlab.inria.fr/spring/wp3_av_perception/body_to_face_mapper](node associates detected bodies to detected faces in image-space. REPO:https://gitlab.inria.fr/spring/wp3_av_perception/body_to_face_mapper)


#### Inputs

 - Topic subscription: `/humans/faces/tracked [hri_msgs/IdsList]`

 - Topic subscription: `/humans/bodies/tracked [hri_msgs/IdsList]`

 - Topic subscription: `/humans/faces/TEST_ID_FACE/roi [hri_msgs/NormalizedRegionOfInterest2D]`

 - Topic subscription: `/humans/bodies/<id>/roi [hri_msgs/NormalizedRegionOfInterest2D]`


#### Outputs

 - Topic publication: `/humans/candidate_matches [hri_msgs/IdsMatch]`


#### Dependencies

- `hri_msgs/IdsList`
- `hri_msgs/IdsMatch`
- `hri_msgs/NormalizedRegionOfInterest2D`




### front_fisheye_2d_body_pose_detector_op {#front_fisheye_2d_body_pose_detector_op}

 Based on OpenPOSE 

*The node front_fisheye_2d_body_pose_detector_op (id: `front_fisheye_2d_body_pose_detector_op`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp3_av_perception/front_fisheye_2d_body_pose_detector](https://gitlab.inria.fr/spring/wp3_av_perception/front_fisheye_2d_body_pose_detector)


#### Inputs

 - Topic subscription: `/tracker/tracker_output [std_msgs/String]`

 - Topic subscription: `/front_camera/fisheye/image_raw [sensor_msgs/Image]`


#### Outputs

 - Topic publication: `/humans/bodies/<id>/skeleton2d [hri_msg/Skeleton2D]`


#### Dependencies

- `hri_msg/Skeleton2D`
- `std_msgs/String`
- `sensor_msgs/Image`




### front_fisheye_2d_body_tracker {#front_fisheye_2d_body_tracker}



*The node front_fisheye_2d_body_tracker (id: `front_fisheye_2d_body_tracker`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: devel

Source code repository: [ https://gitlab.inria.fr/spring/wp3_av_perception/multi-person_visual_tracker/]( https://gitlab.inria.fr/spring/wp3_av_perception/multi-person_visual_tracker/)


#### Inputs

 - Topic subscription: `/front_camera_basetation/fisheye/image_raw/compressed [sensor_msgs/CompressedImage]`


#### Outputs

 - Output: 
 - Topic publication: `/humans/bodies/<id>/cropped [sensor_msgs/Image]`

 - Topic publication: `/tracker/tracker_output [std_msgs/String]`

 - Topic publication: `/humans/bodies/<id>/roi [hri_msgs/NormalizedRegionOfInterest2D]`


#### Dependencies

- `std_msgs/Empty`
- `sensor_msgs/Image`
- `sensor_msgs/CompressedImage`
- `std_msgs/String`
- `hri_msgs/NormalizedRegionOfInterest2D`




### go_to_body_action_server {#go_to_body_action_server}

  

*The node go_to_body_action_server (id: `go_to_body_action_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/go_towards [GoTowards/GoTowards]`


#### Dependencies

- `std_msgs/Empty`
- `GoTowards/GoTowards`
- `ControllerStatus/ControllerStatus`




### go_to_group_action_server {#go_to_group_action_server}

  

*The node go_to_group_action_server (id: `go_to_group_action_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/go_towards [GoTowards/GoTowards]`


#### Dependencies

- `GoTowards/GoTowards`
- `std_msgs/Empty`
- `ControllerStatus/ControllerStatus`




### go_to_person_action_server {#go_to_person_action_server}

  

*The node go_to_person_action_server (id: `go_to_person_action_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/go_towards [GoTowards/GoTowards]`


#### Dependencies

- `GoTowards/GoTowards`
- `std_msgs/Empty`
- `ControllerStatus/ControllerStatus`




### go_to_position_action_server {#go_to_position_action_server}

  

*The node go_to_position_action_server (id: `go_to_position_action_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/go_towards [GoTowards/GoTowards]`


#### Dependencies

- `std_msgs/Empty`
- `GoTowards/GoTowards`
- `ControllerStatus/ControllerStatus`




### group_detector {#group_detector}



*The node group_detector (id: `group_detector`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp4_behavior/group_detector](https://gitlab.inria.fr/spring/wp4_behavior/group_detector)


#### Inputs

 - Input: /h/p/tracked
 - Input: `tf: /person_*` (tf) 

#### Outputs

 - Topic publication: `/h/g/tracked [hri_msgs/IdList]`

 - Output: `tf: /group_*` (tf) 
 - Topic publication: `/humans/group/<id>/ [hri_msgs/IdList]`


#### Dependencies

- `hri_msgs/IdList`
- `tf/transform_broadcaster`
- `std_msgs/Empty`
- `tf/transform_listener`




### look_at_action_server {#look_at_action_server}

  

*The node look_at_action_server (id: `look_at_action_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/look_at [LookAt/LookAt]`


#### Dependencies

- `std_msgs/Empty`
- `LookAt/LookAt`
- `ControllerStatus/ControllerStatus`




### look_at_person_server {#look_at_person_server}



*The node look_at_person_server (id: `look_at_person_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/look_at [LookAt/LookAt]`


#### Dependencies

- `std_msgs/Empty`
- `LookAt/LookAt`
- `ControllerStatus/ControllerStatus`




### look_at_position_action_server {#look_at_position_action_server}



*The node look_at_position_action_server (id: `look_at_position_action_server`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: goal
 - Topic subscription: `/controller_status [ControllerStatus/ControllerStatus]`


#### Outputs

 - Topic publication: `/navigate [Navigate/Navigate]`


#### Dependencies

- `std_msgs/Empty`
- `Navigate/Navigate`
- `ControllerStatus/ControllerStatus`




### occupancy_map_republisher {#occupancy_map_republisher}



*The node occupancy_map_republisher (id: `occupancy_map_republisher`) is maintained by INRIA.*

#### Status

This node is not yet implemented.


#### Inputs

 - Topic subscription: `/slam/obstacle_map [OccupancyGrid/OccupancyGrid]`

 - Topic subscription: `/slam/occupancy_map [OccupancyGrid/OccupancyGrid]`


#### Outputs

 - Topic publication: `/slam/local_map [OccupancyGrid/OccupancyGrid]`


#### Dependencies

- `OccupancyGrid/OccupancyGrid`




### pointcloud2occmap {#pointcloud2occmap}



*The node pointcloud2occmap (id: `pointcloud2occmap`) is maintained by INRIA.*

#### Status

This node is not yet implemented.


#### Inputs

 - Input: torso pointcloud

#### Outputs

 - Topic publication: `/slam/obstacle_map [OccupancyGrid/OccupancyGrid]`


#### Dependencies

- `std_msgs/Empty`
- `OccupancyGrid/OccupancyGrid`




### social_mpc {#social_mpc}

 The code is primarily developed at INRIA by Timothée Wintz.

*The node social_mpc (id: `social_mpc`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: devel

Source code repository: [ https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior]( https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior)


#### Inputs

 - Input: /h/p/tracked
 - Topic subscription: `/look_at [LookAt/LookAt]`

 - Topic subscription: `/go_towards [GoTowards/GoTowards]`

 - Input: `tf: /body_*` (tf) 
 - Input: /h/b/tracked
 - Input: `tf: /person_*` (tf) 
 - Topic subscription: `/navigate [Navigate/Navigate]`

 - Topic subscription: `/slam/local_map [OccupancyGrid/OccupancyGrid]`

 - Input: /h/g/tracked
 - Input: /joint_states
 - Input: `tf: /group_*` (tf) 

#### Outputs

 - Output: /controller_status
 - Output: `tf: /final_point /nav_goal... ` (tf) 
 - Topic publication: `/nav_vel [Twist/Twist]`

 - Topic publication: `/head_controller/command [JointTrajectory/JointTrajectory]`


#### Dependencies

- `std_msgs/Empty`
- `LookAt/LookAt`
- `GoTowards/GoTowards`
- `tf/transform_listener`
- `Navigate/Navigate`
- `OccupancyGrid/OccupancyGrid`
- `tf/transform_broadcaster`
- `Twist/Twist`
- `JointTrajectory/JointTrajectory`




### voicebodycandidatematches {#voicebodycandidatematches}

  

*The node voicebodycandidatematches (id: `voicebodycandidatematches`) is maintained by INRIA.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: /humans/voices/tracked
 - Input: `tf: /voice_*` (tf) 
 - Input: /humans/bodies/tracked
 - Input: `tf: /body_*` (tf) 

#### Outputs

 - Topic publication: `/humans/candidate_matches [hri_msgs/IdsMatch]`


#### Dependencies

- `std_msgs/Empty`
- `tf/transform_listener`
- `hri_msgs/IdsMatch`




## Other



## PAL



### fisheye {#fisheye}



*The node fisheye (id: `fisheye`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs


#### Outputs

 - Topic publication: `/torso_front_camera/color/image_raw [sensor_msgs/Image]`


#### Dependencies

- `sensor_msgs/Image`




### hri_person_manager {#hri_person_manager}



*The node hri_person_manager (id: `hri_person_manager`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: master

Source code repository: [https://gitlab.inria.fr/spring/wp7_ari/hri_person_manager.git](https://gitlab.inria.fr/spring/wp7_ari/hri_person_manager.git)


#### Inputs

 - Topic subscription: `/humans/candidate_matches [hri_msgs/IdsMatch]`


#### Outputs

 - Output: /h/p/...
 - Topic publication: `/h/p/tracked [hri_msgs/IdsList]`

 - Output: `tf: /person_*` (tf) 

#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/IdsList`
- `tf/transform_broadcaster`
- `hri_msgs/IdsMatch`




### knowledge_core {#knowledge_core}



*The node knowledge_core (id: `knowledge_core`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: 2.8.0

Source code repository: [https://gitlab.inria.fr/spring/wp7_ari/knowledge_core](https://gitlab.inria.fr/spring/wp7_ari/knowledge_core)


#### Inputs

 - Topic subscription: `/kb/add_fact [std_msgs/String]`


#### Outputs

 - Output:  /kb/query [service]

#### Dependencies

- `std_msgs/String`
- `std_msgs/Empty`




### people_facts {#people_facts}



*The node people_facts (id: `people_facts`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: 0.2.2

Source code repository: [https://gitlab.inria.fr/spring/wp7_ari/people_facts](https://gitlab.inria.fr/spring/wp7_ari/people_facts)


#### Inputs

 - Input: /h/p/...

#### Outputs

 - Topic publication: `/kb/add_fact [std_msgs/String]`


#### Dependencies

- `std_msgs/Empty`
- `std_msgs/String`




### raspicam {#raspicam}



*The node raspicam (id: `raspicam`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs


#### Outputs

 - Topic publication: `/head_front_camera/color/image_raw [sensor_msgs/Image]`


#### Dependencies

- `sensor_msgs/Image`




### respeaker_ros {#respeaker_ros}



*The node respeaker_ros (id: `respeaker_ros`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: master

Source code repository: []()


#### Inputs


#### Outputs

 - Topic publication: `/audio/raw_audio [audio_common_msgs/AudioData]`


#### Dependencies

- `audio_common_msgs/AudioData`




### Robot functional layer {#robotfunctionallayer}



*The node Robot functional layer (id: `robotfunctionallayer`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Input: input
 - Topic subscription: `/tts [action_server/action_server]`


#### Outputs

 - Output: /joint_states

#### Dependencies

- `std_msgs/Empty`
- `action_server/action_server`




### torso_rgbd_camera {#torso_rgbd_camera}



*The node torso_rgbd_camera (id: `torso_rgbd_camera`) is maintained by PAL.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs


#### Outputs

 - Output: pointcloud
 - Output: torso_front_camera/infra*/*
 - Output: /torso_front_camera/imu
 - Topic publication: `/torso_front_camera/color/image_raw [sensor_msgs/Image]`


#### Dependencies

- `std_msgs/Empty`
- `sensor_msgs/Image`




## UNITN



### activity_recognition {#activity_recognition}

  

*The node activity_recognition (id: `activity_recognition`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Topic subscription: `/vision_msgs/human_2d_pose [human_2d_pose_estimation/Frame]`


#### Outputs

 - Output: [?] output to be defined + added to hri_msgs if possible

#### Dependencies

- `std_msgs/Empty`
- `human_2d_pose_estimation/Frame`




### depth_estimation {#depth_estimation}



*The node depth_estimation (id: `depth_estimation`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp4_behavior/depth-estimation](https://gitlab.inria.fr/spring/wp4_behavior/depth-estimation)


#### Inputs

 - Topic subscription: `/*_basestation/head_front_camera/... [sensor_msgs/Image]`


#### Outputs

 - Topic publication: `/depth_estimation [sensor_msgs/Image]`


#### Dependencies

- `sensor_msgs/Image`




### emotion_estimation {#emotion_estimation}

  

*The node emotion_estimation (id: `emotion_estimation`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: []()


#### Inputs

 - Topic subscription: `/humans/faces/TEST_ID_FACE/cropped [sensor_msg/Image]`


#### Outputs

 - Topic publication: `/humans/faces/TEST_ID_FACE/expression [hri_msgs/Expression]`


#### Dependencies

- `sensor_msg/Image`
- `hri_msgs/Expression`




### face_tracker {#face_tracker}



*The node face_tracker (id: `face_tracker`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: 0.0.1

Source code repository: [ https://gitlab.inria.fr/spring/wp4_behavior/face-tracker]( https://gitlab.inria.fr/spring/wp4_behavior/face-tracker)


#### Inputs

 - Topic subscription: `/*_basestation/head_front_camera/... [sensor_msgs/Image]`


#### Outputs

 - Topic publication: `/humans/faces/TEST_ID_FACE/cropped [sensor_msg/Image]`

 - Topic publication: `/humans/faces/TEST_ID_FACE/roi [hri_msgs/NormalizedRegionOfInterest2D]`

 - Topic publication: `/humans/faces/tracked [hri_msgs/IdsList]`


#### Dependencies

- `sensor_msg/Image`
- `sensor_msgs/Image`
- `hri_msgs/NormalizedRegionOfInterest2D`
- `hri_msgs/IdsList`




### gaze_estimation {#gaze_estimation}



*The node gaze_estimation (id: `gaze_estimation`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: devel

Source code repository: []()


#### Inputs

 - Topic subscription: `/*_basestation/head_front_camera/... [sensor_msgs/Image]`

 - Topic subscription: `/depth_estimation [sensor_msgs/Image]`

 - Topic subscription: `/humans/faces/TEST_ID_FACE/roi [sensor_msgs/RegionOfInterest]`


#### Outputs

 - Output: GazeFrame [2D point in rgb frame]

#### Dependencies

- `sensor_msgs/Image`
- `std_msgs/Empty`
- `sensor_msgs/RegionOfInterest`




### human_2d_pose_estimation {#human_2d_pose_estimation}



*The node human_2d_pose_estimation (id: `human_2d_pose_estimation`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: main

Source code repository: [https://gitlab.inria.fr/spring/wp4_behavior/human-2d-pose-estimation](https://gitlab.inria.fr/spring/wp4_behavior/human-2d-pose-estimation)


#### Inputs

 - Topic subscription: `/*_basestation/head_front_camera/... [sensor_msgs/Image]`


#### Outputs

 - Topic publication: `/vision_msgs/human_2d_pose [human_2d_pose_estimation/Frame]`


#### Dependencies

- `sensor_msgs/Image`
- `human_2d_pose_estimation/Frame`




### mask_detector {#mask_detector}



*The node mask_detector (id: `mask_detector`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: master

Source code repository: [https://gitlab.inria.fr/spring/wp4_behavior/mask-detection BIN:mask_detector.py](https://gitlab.inria.fr/spring/wp4_behavior/mask-detection BIN:mask_detector.py)


#### Inputs

 - Topic subscription: `/humans/faces/TEST_ID_FACE/cropped [sensor_msg/Image]`


#### Outputs

 - Topic publication: `/humans/faces/TEST_ID_FACE/has_mask [wp4_msgs/FaceMask]`


#### Dependencies

- `sensor_msg/Image`
- `wp4_msgs/FaceMask`




### soft_biometrics_estimator {#soft_biometrics_estimator}



*The node soft_biometrics_estimator (id: `soft_biometrics_estimator`) is maintained by UNITN.*

#### Status

Implemented. Current release/branch: master

Source code repository: [https://gitlab.inria.fr/spring/wp4_behavior/wp4_behavior_understanding SUBFOLDER:wp4_people_characteristics BIN:soft_biometrics_estimator.py](https://gitlab.inria.fr/spring/wp4_behavior/wp4_behavior_understanding SUBFOLDER:wp4_people_characteristics BIN:soft_biometrics_estimator.py)


#### Inputs

 - Topic subscription: `/*_basestation/head_front_camera/... [sensor_msgs/Image]`

 - Topic subscription: `/humans/faces/tracked [hri_msgs/IdsList]`

 - Topic subscription: `/humans/faces/TEST_ID_FACE/roi [hri_msgs/NormalizedRegionOfInterest2D]`


#### Outputs

 - Topic publication: `/humans/candidate_matches [hri_msgs/IdsMatch] [face reco/face reco]`

 - Topic publication: `/humans/faces/TEST_ID_FACE/softbiometrics [hri_msgs/SoftBiometrics]`


#### Dependencies

- `face reco/face reco`
- `sensor_msgs/Image`
- `hri_msgs/IdsList`
- `hri_msgs/NormalizedRegionOfInterest2D`
- `hri_msgs/SoftBiometrics`





