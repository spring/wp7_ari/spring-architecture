# SPRING architecture

**Version:** 0.5.0

EU H2020 SPRING architecture

## Overview of modules

### High priority for Integration Week

- 🟩: ready/released
- ⏱️: needs update
- 🟧: not released yet
- 🟥: not started yet
- ❔: status unknown

| **Status** | **Node** | **id** | **Partner** | **Status** |
|------------|----------|--------|-------------|------------|
| 🟧 | [ InLoc Server](#inlocserver) | inlocserver | CVUT | mock-up |
| ❔ | [InLoc-ROS](#inlocros) | inlocros | CVUT | mock-up |
| 🟧 | [Object detection/identification/localisation](#objectdetectionidentificationlocalisation) | objectdetectionidentificationlocalisation | CVUT | mock-up |
| 🟩 | [MoDE](#mode) | mode | BIU | released (version BIU_dev) |
| ❔ | [Multi-person audio tracking](#multipersonaudiotracking) | multipersonaudiotracking | BIU | mock-up |
| 🟩 | [google_asr](#google_asr) | google_asr | BIU | released (version BIU_dev) |
| ❔ | [Speaker identification](#speakeridentification) | speakeridentification | BIU | mock-up |
| 🟥 | [Voice speech matching](#voicespeechmatching) | voicespeechmatching | BIU | mock-up |
| 🟧 | [Face detection](#facedetection) | facedetection | UNITN | mock-up |
| 🟧 | [Person re-identification](#personreidentification) | personreidentification | UNITN | mock-up |
| 🟧 | [Depth estimation from monocular](#depthestimationfrommonocular) | depthestimationfrommonocular | UNITN | mock-up |
| 🟧 | [User attention estimation](#userattentionestimation) | userattentionestimation | UNITN | mock-up |
| 🟧 | [F-formation](#fformation) | fformation | UNITN | mock-up |
| 🟩 | [ros_openpose](#ros_openpose) | ros_openpose | UNITN | released (version 0.0.1) |
| 🟩 | [mask_detector](#mask_detector) | mask_detector | UNITN | released (version master) |
| 🟩 | [soft_biometrics_estimator](#soft_biometrics_estimator) | soft_biometrics_estimator | UNITN | released (version master) |
| 🟩 | [ wp4_msgs](#wp4_msgs) | wp4_msgs | UNITN | released (version master) (dependency) |
| 🟩 | [FairMOT Multi-people body tracker](#fairmotmultipeoplebodytracker) | fairmotmultipeoplebodytracker | INRIA | released (version devel) |
| 🟩 | [People 3D tracker](#people3dtracker) | people3dtracker | INRIA | mock-up |
| 🟩 | [interaction_manager](#interaction_manager) | interaction_manager | HWU | released (version spring_dev) |
| 🟩 | [ interaction_manager_msgs](#interaction_manager_msgs) | interaction_manager_msgs | HWU | released (version spring_dev) (dependency) |
| 🟩 | [ robot_behaviour_msgs](#robot_behaviour_msgs) | robot_behaviour_msgs | HWU | released (version spring_dev) (dependency) |
| 🟩 | [dialogue arbiter](#dialoguearbiter) | dialoguearbiter | HWU | released (version spring_dev) |
| ❔ | [ audio_msgs](#audio_msgs) | audio_msgs | HWU | released (version spring_dev) (dependency) |
| 🟩 | [ social_scene_msgs](#social_scene_msgs) | social_scene_msgs | HWU | released (version spring_dev) (dependency) |
| ❔ | [social_scene_context_understanding](#social_scene_context_understanding) | social_scene_context_understanding | HWU | released (version spring_dev) |
| 🟩 | [robot_behaviour_plan_actions](#robot_behaviour_plan_actions) | robot_behaviour_plan_actions | HWU | released (version spring_dev) |
| 🟩 | [robot_behavior](#robot_behavior) | robot_behavior | INRIA | released (version devel) |
| 🟧 | [Robot GUI](#robotgui) | robotgui | ERM | mock-up |
| 🟩 | [hri_person_manager](#hri_person_manager) | hri_person_manager | PAL | released (version master) |
| 🟩 | [ hri_msgs](#hri_msgs) | hri_msgs | PAL | released (version 0.1.1) (dependency) |
| 🟩 | [raspicam](#raspicam) | raspicam | PAL | mock-up |
| 🟩 | [ORB SLAM](#orbslam) | orbslam | PAL | mock-up |
| 🟩 | [torso_rgbd_camera](#torso_rgbd_camera) | torso_rgbd_camera | PAL | mock-up |
| 🟩 | [ spring_msgs](#spring_msgs) | spring_msgs | PAL | released (version 0.0.2) |
| 🟩 | [Speech synthesis](#speechsynthesis) | speechsynthesis | PAL | mock-up |
| 🟩 | [respeaker_ros](#respeaker_ros) | respeaker_ros | PAL | released (version master) |
| 🟩 | [fisheye](#fisheye) | fisheye | PAL | mock-up |
| 🟩 | [Robot functional layer](#robotfunctionallayer) | robotfunctionallayer | PAL | mock-up |

### Lower priority

| **Status** | **Node** | **id** | **Partner** | **Status** |
|------------|----------|--------|-------------|------------|
| ❔ | [Semantic mapping](#semanticmapping) | semanticmapping | CVUT | mock-up |
| 🟥 | [Occupancy map](#occupancymap) | occupancymap | CVUT | mock-up |
| ❔ | [Non-verbal behaviours](#nonverbalbehaviours) | nonverbalbehaviours | UNITN | mock-up |
| ❔ | [User visual focus](#uservisualfocus) | uservisualfocus | UNITN | mock-up |
| 🟥 | [Activity reco](#activityreco) | activityreco | UNITN | mock-up |
| ❔ | [ROS mediapipe/openpose](#rosmediapipeopenpose) | rosmediapipeopenpose | INRIA | mock-up |

## Detailed description



---

### personreidentification

Node *Person re-identification* (id: `personreidentification`) is overseen by UNITN.

MOCK: Person re-identification is...

#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/h/f/*/roi [sensor_msgs/RegionOfInterest]` (topic)

 - Output: `person_id_candidate [hri_msgs/IdsMatch]` (undefined)

#### Dependencies

- `std_msgs/Empty`
- `sensor_msgs/RegionOfInterest`


---

### nonverbalbehaviours

Node *Non-verbal behaviours* (id: `nonverbalbehaviours`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/h/f/*/roi [hri_msgs/RegionOfInterest]` (topic)
 - Input: `/h/v/*/audio [audio_common_msgs/AudioData]` (topic)

 - Output: `/h/f/*/expression [hri_msgs/Expression]` (topic)

#### Dependencies

- `hri_msgs/RegionOfInterest`
- `hri_msgs/Expression`
- `audio_common_msgs/AudioData`


---

### torso_rgbd_camera

Node *torso_rgbd_camera* (id: `torso_rgbd_camera`) is overseen by PAL.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs


 - Output: `/torso_rgbd_camera/color/image_raw [sensor_msgs/Image]` (topic)

#### Dependencies

- `sensor_msgs/Image`


---

### inlocserver

Node * InLoc Server* (id: `inlocserver`) is overseen by CVUT.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs



#### Dependencies



---

### spring_msgs

Node * spring_msgs* (id: `spring_msgs`) is overseen by PAL.

REPO:git@gitlab.inria.fr:spring/wp7_ari/spring_msgs.git NOT EXECUTABLE

#### Status

**Current release: 0.0.2** 

#### Inputs/outputs



#### Dependencies



---

### userattentionestimation

Node *User attention estimation* (id: `userattentionestimation`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `TF (faces)` (undefined)
 - Input: `/h/f/*/roi [hri_msgs/RegionOfInterest]` (topic)

 - Output: `tf: /face_id_gaze` (tf)
 - Output: `x,y + attention heatmap` (undefined)

#### Dependencies

- `tf/transform_broadcaster`
- `std_msgs/Empty`
- `hri_msgs/RegionOfInterest`


---

### interaction_manager

Node *interaction_manager* (id: `interaction_manager`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/interaction.git
SUBFOLDER:interaction_manager

#### Status

**Current release: spring_dev** 

#### Inputs/outputs

 - Input: `semantic scene description` (undefined)
 - Input: `/h/p/*/softbiometrics [hri_msgs/Softbiometrics]` (topic)
 - Input: `TF` (undefined)
 - Input: `robot state` (undefined)
 - Input: `dialogue state` (undefined)

 - Output: `verbal command` (undefined)
 - Output: `nav goals` (undefined)
 - Output: `who to look at` (undefined)
 - Output: `active personID` (undefined)
 - Output: `gestures` (undefined)

#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/Softbiometrics`


---

### objectdetectionidentificationlocalisation

Node *Object detection/identification/localisation* (id: `objectdetectionidentificationlocalisation`) is overseen by CVUT.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/camera_head/color/image_raw [sensor_msgs/Image]` (topic)

 - Output: `/detected_objects [spring_msgs/DetectedObjectArray]` (topic)

#### Dependencies

- `spring_msgs/DetectedObjectArray`
- `sensor_msgs/Image`


---

### voicespeechmatching

Node *Voice speech matching* (id: `voicespeechmatching`) is overseen by BIU.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/audio/speech_streams [std_msgs/String]` (topic)
 - Input: `/h/v/*/audio [audio_common_msgs/AudioData]` (topic)

 - Output: `/h/v/*/speech [hri_msgs/LiveSpeech]` (topic)

#### Dependencies

- `hri_msgs/LiveSpeech`
- `std_msgs/String`
- `audio_common_msgs/AudioData`


---

### speakeridentification

Node *Speaker identification* (id: `speakeridentification`) is overseen by BIU.

- online services
- not started yet

#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `tracking information` (undefined)
 - Input: `/audio/postprocess_audio_streams [audio_common_msgs/AudioData]` (topic)

 - Output: `person_id_candidate [hri_msgs/IdsMatch]` (undefined)
 - Output: `/h/v/*/audio [audio_common_msgs/AudioData]` (topic)

#### Dependencies

- `std_msgs/Empty`
- `audio_common_msgs/AudioData`


---

### mask_detector

Node *mask_detector* (id: `mask_detector`) is overseen by UNITN.

Detects presence of a facial mask
REPO:git@gitlab.inria.fr:spring/wp4_behavior/wp4_behavior_understanding.git
SUBFOLDER:wp4_people_characteristics
BIN:mask_detector.py

#### Status

**Current release: master** 

#### Inputs/outputs

 - Input: `/h/f/*/roi [sensor_msgs/RegionOfInterest]` (topic)

 - Output: `/h/f/*/mask [std_msgs/Bool]` (topic)

#### Dependencies

- `sensor_msgs/RegionOfInterest`
- `std_msgs/Bool`


---

### hri_person_manager

Node *hri_person_manager* (id: `hri_person_manager`) is overseen by PAL.

REPO:git@gitlab.inria.fr:spring/wp7_ari/hri_person_manager.git

#### Status

**Current release: master** 

#### Inputs/outputs

 - Input: `TF (faces)` (undefined)
 - Input: `/h/f/*/softbiometrics [hri_msgs/Softbiometrics]` (topic)
 - Input: `candidate_matches [hri_msgs/IdsMatch]` (undefined)
 - Input: `TF (voices)` (undefined)

 - Output: `/h/p/*/voice_id [std_msgs/String]` (topic)
 - Output: `/h/p/*/softbiometrics [hri_msgs/Softbiometrics]` (topic)
 - Output: `/h/p/*/face_id [std_msgs/String]` (topic)
 - Output: `/humans/persons/*/body_id [std_msgs/String]` (topic)
 - Output: `tf: /person_id` (tf)

#### Dependencies

- `std_msgs/String`
- `std_msgs/Empty`
- `hri_msgs/Softbiometrics`
- `tf/transform_broadcaster`


---

### soft_biometrics_estimator

Node *soft_biometrics_estimator* (id: `soft_biometrics_estimator`) is overseen by UNITN.

Detects age/gender
REPO:git@gitlab.inria.fr:spring/wp4_behavior/wp4_behavior_understanding.git
SUBFOLDER:wp4_people_characteristics
BIN:soft_biometrics_estimator.py

#### Status

**Current release: master** 

#### Inputs/outputs

 - Input: `/h/f/*/roi [hri_msgs/RegionOfInterest]` (topic)

 - Output: `/h/f/*/softbiometrics [hri_msgs/Softbiometrics]` (topic)

#### Dependencies

- `hri_msgs/RegionOfInterest`
- `hri_msgs/Softbiometrics`


---

### rosmediapipeopenpose

Node *ROS mediapipe/openpose* (id: `rosmediapipeopenpose`) is overseen by INRIA.

MOCK: ROS mediapipe/openpose is...

#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `input` (undefined)

 - Output: `output` (undefined)

#### Dependencies

- `std_msgs/Empty`


---

### people3dtracker

Node *People 3D tracker* (id: `people3dtracker`) is overseen by INRIA.

MOCK: People 3D tracker [position/orientation] is...

#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `ground plane` (undefined)
 - Input: `people RoIs` (undefined)
 - Input: `feet position` (undefined)

 - Output: `tf: /body_id` (tf)

#### Dependencies

- `std_msgs/Empty`
- `tf/transform_broadcaster`


---

### fformation

Node *F-formation* (id: `fformation`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/h/i/gaze [hri_msgs/Gaze]` (topic)
 - Input: `tf: /person_id` (tf)

 - Output: `/h/i/groups [hri_msgs/Group]` (topic)

#### Dependencies

- `hri_msgs/Group`
- `hri_msgs/Gaze`
- `tf/transform_listener`


---

### respeaker_ros

Node *respeaker_ros* (id: `respeaker_ros`) is overseen by PAL.

REPO:git@gitlab.inria.fr:spring/wp7_ari/respeaker_ros.git BIN:respeaker_multichan_node.py

#### Status

**Current release: master** 

#### Inputs/outputs


 - Output: `/audio/ego_audio [audio_common_msgs/AudioData]` (topic)
 - Output: `/audio/raw_audio [respeaker_ros/RawAudioData]` (topic)

#### Dependencies

- `audio_common_msgs/AudioData`
- `respeaker_ros/RawAudioData`


---

### fisheye

Node *fisheye* (id: `fisheye`) is overseen by PAL.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs


 - Output: `/torso_front_camera/color/image_raw [sensor_msgs/Image]` (topic)

#### Dependencies

- `sensor_msgs/Image`


---

### dialoguearbiter

Node *dialogue arbiter* (id: `dialoguearbiter`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/dialogue.git
SUBFOLDER:dialogue_arbiter

#### Status

**Current release: spring_dev** 

#### Inputs/outputs

 - Input: `interaction messages` (undefined)
 - Input: `/h/v/*/speech [hri_msgs/LiveSpeech]` (topic)

 - Output: `next utterance` (undefined)
 - Output: `DialogueState` (undefined)

#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/LiveSpeech`


---

### facedetection

Node *Face detection* (id: `facedetection`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/camera_head/color/image_raw [sensor_msgs/Image]` (topic)

 - Output: `/h/f/*/cropped [sensor_msg/Image]` (topic)
 - Output: `/h/f/*/roi [sensor_msgs/RegionOfInterest]` (topic)

#### Dependencies

- `sensor_msg/Image`
- `sensor_msgs/RegionOfInterest`
- `sensor_msgs/Image`


---

### depthestimationfrommonocular

Node *Depth estimation from monocular* (id: `depthestimationfrommonocular`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `input` (undefined)

 - Output: `depth` (undefined)

#### Dependencies

- `std_msgs/Empty`


---

### robotfunctionallayer

Node *Robot functional layer* (id: `robotfunctionallayer`) is overseen by PAL.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `input` (undefined)


#### Dependencies

- `std_msgs/Empty`


---

### fairmotmultipeoplebodytracker

Node *FairMOT Multi-people body tracker* (id: `fairmotmultipeoplebodytracker`) is overseen by INRIA.

This code is primarily developed at INRIA by Luis Gomez Camara.
REPO: https://gitlab.inria.fr/spring/wp3_av_perception/multi-person_visual_tracker/

#### Status

**Current release: devel** 

#### Inputs/outputs

 - Input: `/camera_head/color/image_raw [sensor_msgs/Image]` (topic)

 - Output: `/h/b/*/cropped [sensor_msg/Image]` (topic)
 - Output: `/h/b/*/roi [sensor_msgs/RegionOfInterest` (undefined)

#### Dependencies

- `sensor_msg/Image`
- `sensor_msgs/Image`
- `std_msgs/Empty`


---

### mode

Node *MoDE* (id: `mode`) is overseen by BIU.

This node does:
- speech echo cancelation,
- speech enhancement,
- speech separation and diarization
REPO:https://gitlab.inria.fr/spring/wp3_av_perception/speech-enhancement
SUBFOLDER:audio_processing 

#### Status

**Current release: BIU_dev** 

#### Inputs/outputs

 - Input: `sound localization` (undefined)
 - Input: `/audio/ego_audio [audio_common_msgs/AudioData]` (topic)
 - Input: `/audio/raw_audio [respeaker_ros/RawAudioData]` (topic)

 - Output: `/audio/postprocess_audio_streams [audio_common_msgs/AudioData]` (topic)

#### Dependencies

- `audio_common_msgs/AudioData`
- `std_msgs/Empty`
- `respeaker_ros/RawAudioData`


---

### speechsynthesis

Node *Speech synthesis* (id: `speechsynthesis`) is overseen by PAL.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `speech [std_msgs/String]` (undefined)

 - Output: `/tts/feedback` (undefined)

#### Dependencies

- `std_msgs/Empty`


---

### uservisualfocus

Node *User visual focus* (id: `uservisualfocus`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `scene` (undefined)
 - Input: `attention` (undefined)
 - Input: `depth` (undefined)
 - Input: `gaze direction` (undefined)

 - Output: `who's looking at what?` (undefined)
 - Output: `/h/i/gaze [hri_msgs/Gaze]` (topic)

#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/Gaze`


---

### orbslam

Node *ORB SLAM* (id: `orbslam`) is overseen by PAL.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/camera_torso/color/image_raw [sensor_msgs/Image]` (topic)

 - Output: `/map [nav_msgs/OccupancyGrid]` (topic)
 - Output: `tf: /odom` (tf)

#### Dependencies

- `nav_msgs/OccupancyGrid`
- `tf/transform_broadcaster`
- `sensor_msgs/Image`


---

### robot_behavior

Node *robot_behavior* (id: `robot_behavior`) is overseen by INRIA.

The code is primarily developed at INRIA by Timothée Wintz.
REPO: https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior
SUBFOLDER:src/robot_behavior

#### Status

**Current release: devel** 

#### Inputs/outputs

 - Input: `following/nav goals` (undefined)
 - Input: `status` (undefined)
 - Input: `/h/i/groups [hri_msgs/Group]` (topic)
 - Input: `look at` (undefined)
 - Input: `occupancy map` (undefined)

 - Output: `status` (undefined)
 - Output: `low-level actions` (undefined)

#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/Group`


---

### semanticmapping

Node *Semantic mapping* (id: `semanticmapping`) is overseen by CVUT.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `dense 3d map` (undefined)
 - Input: `/detected_objects [spring_msgs/DetectedObjectArray]` (topic)

 - Output: `scene graph` (undefined)

#### Dependencies

- `std_msgs/Empty`
- `spring_msgs/DetectedObjectArray`


---

### social_scene_context_understanding

Node *social_scene_context_understanding* (id: `social_scene_context_understanding`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/interaction.git
SUBFOLDER:social_scene_context_understanding

#### Status

**Current release: spring_dev** 

#### Inputs/outputs

 - Input: `scene graph` (undefined)

 - Output: `semantic description` (undefined)

#### Dependencies

- `std_msgs/Empty`


---

### robotgui

Node *Robot GUI* (id: `robotgui`) is overseen by ERM.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `speech output` (undefined)
 - Input: `additional support material` (undefined)
 - Input: `/h/v/*/speech [hri_msgs/LiveSpeech]` (topic)
 - Input: `/tts/feedback` (undefined)


#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/LiveSpeech`


---

### ros_openpose

Node *ros_openpose* (id: `ros_openpose`) is overseen by UNITN.

REPO:git@gitlab.inria.fr:spring/wp4_behavior/wp4_behavior_understanding.git
SUBFOLDER:ros_openpose

#### Status

**Current release: 0.0.1** 

#### Inputs/outputs

 - Input: `/h/b/*/cropped [sensor_msg/Image]` (topic)

 - Output: `/h/b/*/skeleton2d [hri_msgs/Skeleton2D]` (topic)

#### Dependencies

- `sensor_msg/Image`
- `hri_msgs/Skeleton2D`


---

### activityreco

Node *Activity reco* (id: `activityreco`) is overseen by UNITN.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `TF (bodies)` (undefined)
 - Input: `gaze direction` (undefined)

 - Output: `[?] output` (undefined)

#### Dependencies

- `std_msgs/Empty`


---

### robot_behaviour_plan_actions

Node *robot_behaviour_plan_actions* (id: `robot_behaviour_plan_actions`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/plan_actions.git
SUBFOLDER:robot_behaviour_plan_actions

#### Status

**Current release: spring_dev** 

#### Inputs/outputs

 - Input: `TF (persons)` (undefined)
 - Input: `semantic scene description` (undefined)
 - Input: `dialogue state` (undefined)
 - Input: `/h/p/*/softbiometrics [hri_msgs/Softbiometrics]` (topic)

 - Output: `nav goals` (undefined)
 - Output: `interaction state` (undefined)
 - Output: `output` (undefined)

#### Dependencies

- `std_msgs/Empty`
- `hri_msgs/Softbiometrics`


---

### multipersonaudiotracking

Node *Multi-person audio tracking* (id: `multipersonaudiotracking`) is overseen by BIU.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/h/v/*/audio [audio_common_msgs/AudioData]` (topic)

 - Output: `tracking information` (undefined)
 - Output: `output` (undefined)
 - Output: `source angle` (undefined)
 - Output: `tf: /voice_id` (tf)

#### Dependencies

- `audio_common_msgs/AudioData`
- `std_msgs/Empty`
- `tf/transform_broadcaster`


---

### inlocros

Node *InLoc-ROS* (id: `inlocros`) is overseen by CVUT.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `/camera_torso/color/image_torso [sensor_msgs/Image]` (topic)
 - Input: `/camera_head/color/image_head [sensor_msgs/Image]` (topic)
 - Input: `localisation prior` (undefined)

 - Output: `dense 3d map` (undefined)
 - Output: `tf: /odom` (tf)

#### Dependencies

- `std_msgs/Empty`
- `tf/transform_broadcaster`
- `sensor_msgs/Image`


---

### google_asr

Node *google_asr* (id: `google_asr`) is overseen by BIU.

REPO:https://gitlab.inria.fr/spring/wp5_spoken_conversations/asr
SUBFOLDER:google_asr/google_asr

#### Status

**Current release: BIU_dev** 

#### Inputs/outputs

 - Input: `/audio/postprocess_audio_streams [audio_common_msgs/AudioData]` (topic)

 - Output: `/audio/speech_streams -- array of hri_msgs/LiveSpeech` (undefined)

#### Dependencies

- `audio_common_msgs/AudioData`
- `std_msgs/Empty`


---

### occupancymap

Node *Occupancy map* (id: `occupancymap`) is overseen by CVUT.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs

 - Input: `dense 3d map` (undefined)
 - Input: `TF (bodies)` (undefined)

 - Output: `/map_refined [nav_msgs/OccupancyGrid]` (topic)

#### Dependencies

- `std_msgs/Empty`
- `nav_msgs/OccupancyGrid`


---

### raspicam

Node *raspicam* (id: `raspicam`) is overseen by PAL.



#### Status

**This node is currently auto-generated (mock-up)** 

#### Inputs/outputs


 - Output: `/head_front_camera/color/image_raw [sensor_msgs/Image]` (topic)

#### Dependencies

- `sensor_msgs/Image`



### Non-executable dependency:  interaction_manager_msgs

Module  interaction_manager_msgs (id: `interaction_manager_msgs`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/interaction.git
SUBFOLDER:interaction_manager_msgs
NOT EXECUTABLE


#### Status

**Current release: spring_dev** 

#### Dependencies





### Non-executable dependency:  hri_msgs

Module  hri_msgs (id: `hri_msgs`) is overseen by PAL.

REPO: git@gitlab:ros4hri/hri_msgs.git
NOT EXECUTABLE


#### Status

**Current release: 0.1.1** 

#### Dependencies





### Non-executable dependency:  robot_behaviour_msgs

Module  robot_behaviour_msgs (id: `robot_behaviour_msgs`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/plan_actions.git
SUBFOLDER:robot_behaviour_msgs
NOT EXECUTABLE


#### Status

**Current release: spring_dev** 

#### Dependencies





### Non-executable dependency:  audio_msgs

Module  audio_msgs (id: `audio_msgs`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/asr.git
SUBFOLDER:audio_msgs
NOT EXECUTABLE


#### Status

**Current release: spring_dev** 

#### Dependencies





### Non-executable dependency:  social_scene_msgs

Module  social_scene_msgs (id: `social_scene_msgs`) is overseen by HWU.

REPO:git@gitlab.inria.fr:spring/wp5_spoken_conversations/interaction.git
SUBFOLDER:social_scene_msgs
NOT EXECUTABLE


#### Status

**Current release: spring_dev** 

#### Dependencies





### Non-executable dependency:  wp4_msgs

Module  wp4_msgs (id: `wp4_msgs`) is overseen by UNITN.

REPO:git@gitlab.inria.fr:spring/wp4_behavior/wp4_behavior_understanding.git
SUBFOLDER:wp4_msgs
NOT EXECUTABLE


#### Status

**Current release: master** 

#### Dependencies





